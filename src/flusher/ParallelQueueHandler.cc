// ----------------------------------------------------------------------
// File: ParallelQueueHandler.cc
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/

#include "ParallelQueueHandler.hh"
#include "qclient/BackgroundFlusher.hh"

using namespace qclient;

ParallelQueueHandler::ParallelQueueHandler(BackgroundFlusherPersistency * const persistency,
                                           std::unique_ptr<FlusherAckCallback>&& parent_callback,
                                           std::unique_ptr<QClient>&& qclient)
    : persistency(persistency),
      parent_callback(std::move(parent_callback)),
      qclient(std::move(qclient))
{}

void
ParallelQueueHandler::pushRequest(const std::vector<std::string>& operation)
{
  auto index = persistency->record(operation);
  qclient->execute(new StatefulCallback(this, index), operation);
}

void
ParallelQueueHandler::handleAck(ItemIndex index)
{
  persistency->popIndex(index);
  parent_callback->notifyWaiters();
}

void
ParallelQueueHandler::restorefromPersistency()
{
  for (ItemIndex i = persistency->getStartingIndex();
       i != persistency->getEndingIndex(); i++) {
    std::vector<std::string> contents;
    if (!persistency->retrieve(i, contents)) {
      std::cerr << "Skipping item at index=" << i << std::endl;
      continue;
    }
    // This is nasty! currently qclient execute only takes a raw pointer,
    // expecting that callbacks are long lived like the serial case. Since the
    // callback holds no other data we destroy ourselves after we are done. A
    // future option would be using a set of reusable StatefulCallbacks held in
    // list so that we don't need constant allocations & frees

    qclient->execute(new StatefulCallback(this, i), contents);
  }
}

ParallelQueueHandler::StatefulCallback::StatefulCallback(ParallelQueueHandler * parent, ItemIndex index)
    : parent(parent), index(index)
{}

void
ParallelQueueHandler::StatefulCallback::handleResponse(qclient::redisReplyPtr&& reply)
{
  parent->parent_callback->handleResponse(std::move(reply));
  parent->handleAck(index);
  delete this;
}


