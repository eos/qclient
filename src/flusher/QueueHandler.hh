// ----------------------------------------------------------------------
// File: QueueHandler.hh
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/

#ifndef QCLIENT_QUEUEHANDLER_HH
#define QCLIENT_QUEUEHANDLER_HH

#include "qclient/PersistencyLayer.hh"
#include "qclient/QClient.hh"

namespace qclient {

class FlusherAckCallback;

/*
 * QueueHandler is an abstract class that is used to handle the queue operations
 * It is used by the BackgroundFlusher to push operations to the queue
 * and handle acknowledgements. This separation allows for strategies where
 * implementations can decide whether to serially or concurrently push to the
 * queue
 */
class QueueHandler {
public:
  virtual ~QueueHandler() = default;
  virtual void pushRequest(const std::vector<std::string>& operation) = 0;
  virtual void handleAck(ItemIndex index) = 0;
  virtual void restorefromPersistency() = 0;
};

} // namespace qclient

#endif // EOS_QUEUEHANDLER_HH
