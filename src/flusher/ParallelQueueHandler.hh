// ----------------------------------------------------------------------
// File: ParallelQueueHandler.hh
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/


#ifndef EOS_PARALLELQUEUEHANDLER_HH
#define EOS_PARALLELQUEUEHANDLER_HH

#include "QueueHandler.hh"

namespace qclient {

/*
 * ParallelQueueHandler is a QueueHandler implementation that pushes operations
 * to the queue in parallel. Since getStartingIndex cannot be relied upon
 * anymore this delegates the responsibility of tracking the index to the
 * persistency layer which must return the index it has assigned to the
 * operation. This index is then used to pop the operation from the persistency
 * layer when the operation is acked via the callback where the index is passed.
 * There are no locks held in this class and hence can usually perform better
 * than SerialQueueHandler in many cases
 */
class ParallelQueueHandler : public QueueHandler {
public:
  ParallelQueueHandler(BackgroundFlusherPersistency * const persistency,
                       std::unique_ptr<FlusherAckCallback>&& parent_callback,
                       std::unique_ptr<QClient>&& qclient);
  void pushRequest(const std::vector<std::string>& operation) override;
  void handleAck(ItemIndex index) override;
  void restorefromPersistency() override;

  class StatefulCallback : public QCallback {
  public:
    StatefulCallback(ParallelQueueHandler * parent, ItemIndex index);
    void handleResponse(redisReplyPtr &&reply) override;
  private:
    ParallelQueueHandler * parent;
    ItemIndex index;
  };
private:
  BackgroundFlusherPersistency * const persistency;
  std::unique_ptr<FlusherAckCallback> parent_callback;
  std::unique_ptr<QClient> qclient;
};

} // namespace qclient

#endif // EOS_PARALLELQUEUEHANDLER_HH
