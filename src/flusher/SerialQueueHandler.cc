// ----------------------------------------------------------------------
// File: SerialQueueHandler.cc
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/


#include "SerialQueueHandler.hh"
#include "qclient/BackgroundFlusher.hh"

using namespace qclient;

SerialQueueHandler::SerialQueueHandler(BackgroundFlusherPersistency * const persistency,
                                       std::unique_ptr<FlusherAckCallback>&& parent_callback,
                                       std::unique_ptr<QClient>&& qclient)
    : persistency(persistency),
      parent_callback(std::move(parent_callback)),
      qclient(std::move(qclient)),
      callback(this) {}


SerialQueueHandler::FlusherCallback::FlusherCallback(SerialQueueHandler *parent)
    : parent(parent) {}

void SerialQueueHandler::FlusherCallback::handleResponse(redisReplyPtr &&reply) {
  parent->parent_callback->handleResponse(std::move(reply));
  parent->handleAck();
}

void SerialQueueHandler::pushRequest(const std::vector<std::string>& operation)
{
  std::lock_guard lock(newEntriesMtx);
  persistency->record(persistency->getEndingIndex(), operation);
  qclient->execute(&callback, operation);
}

void SerialQueueHandler::handleAck(qclient::ItemIndex)
{
  {
    std::lock_guard lock(newEntriesMtx);
    persistency->pop();
  }
  parent_callback->notifyWaiters();
}

void
SerialQueueHandler::restorefromPersistency()
{
  for (ItemIndex i = persistency->getStartingIndex();
       i != persistency->getEndingIndex(); i++) {
    std::vector<std::string> contents;
    if (!persistency->retrieve(i, contents)) {
      std::cerr << "BackgroundFlusher corruption, could not retrieve entry with index "
                << i << std::endl;
      std::terminate();
    }
    qclient->execute(&callback, contents);
  }
}
