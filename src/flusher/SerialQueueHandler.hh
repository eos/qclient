// ----------------------------------------------------------------------
// File: SerialQueueHandler.hh
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/


#ifndef QCLIENT_SERIALQUEUEHANDLER_HH
#define QCLIENT_SERIALQUEUEHANDLER_HH

#include "QueueHandler.hh"
#include <mutex>

namespace qclient {

/*
 * SerialQueueHandler is a QueueHandler implementation that handles the queue
 * serially, all the pushes are serialized by newEntriesMtx, which also is
 * held by acks, this makes sure all the operations are handled in a consistent
 * order as submitted in the queue. There is no index tracking for popped indices
 * as newEntriesMutex ensures that getStartingIndex is only called by one thread
 * and popIndex is also similarly called with the mutex held.
 */
class SerialQueueHandler : public QueueHandler {
public:
  SerialQueueHandler(BackgroundFlusherPersistency * const persistency,
                     std::unique_ptr<FlusherAckCallback>&& parent_callback,
                     std::unique_ptr<QClient>&& qclient);

  void pushRequest(const std::vector<std::string>& operation) override;
  void handleAck(ItemIndex index = -1) override;
  void restorefromPersistency() override;

  class FlusherCallback : public QCallback {
  public:
    FlusherCallback(SerialQueueHandler *parent);
    virtual ~FlusherCallback() {}
    virtual void handleResponse(redisReplyPtr &&reply) override;

  private:
    SerialQueueHandler *parent;
  };

private:
  // NOTE: It is very important that callback outlives qclient!
  // As qclient would call the callback executor thread to clean up in
  // its destruction. This is the reason qclient cannot be held by the
  // parent classes unless the callback is also held and/or lifetime ensured.
  // Since C++ ensures members are destructed in the reverse order
  // of their declaration, callback is declared before qclient
  // and outlives it. Otherwise, this would lead to segfaults/aborts like
  // pure virtual method called as the QCallback object is invalid

  std::mutex newEntriesMtx;
  BackgroundFlusherPersistency * const persistency;

  FlusherCallback callback;
  std::unique_ptr<FlusherAckCallback> parent_callback;
  std::unique_ptr<QClient> qclient;
};

} // namespace qclient

#endif // EOS_SERIALQUEUEHANDLER_HH
