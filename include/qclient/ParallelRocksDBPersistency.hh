// /************************************************************************
//  * EOS - the CERN Disk Storage System                                   *
//  * Copyright (C) 2024 CERN/Switzerland                           *
//  *                                                                      *
//  * This program is free software: you can redistribute it and/or modify *
//  * it under the terms of the GNU General Public License as published by *
//  * the Free Software Foundation, either version 3 of the License, or    *
//  * (at your option) any later version.                                  *
//  *                                                                      *
//  * This program is distributed in the hope that it will be useful,      *
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
//  * GNU General Public License for more details.                         *
//  *                                                                      *
//  * You should have received a copy of the GNU General Public License    *
//  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
//  ************************************************************************
//

//
// Created by Abhishek Lekshmanan on 18/11/2024.
//

#ifndef QCLIENT_PARALLELROCKSDBPERSISTENCY_HH
#define QCLIENT_PARALLELROCKSDBPERSISTENCY_HH

#include <rocksdb/merge_operator.h>
#include <rocksdb/convenience.h>

#include "qclient/RocksDBPersistency.hh"
#include "utils/AckTracker.hh"

namespace qclient {

// A merge operator allows a check and set operation to be performed atomically
// This way we don't have to do a retrieve from RocksdB, hold a lock and then do
// an update externally in the BackgroundFlusher, but the persistency layer
// implementation itself, ie RocksDBPersistency can do the check and set
// atomically. We are writing a custom merge operator for rocksdb to support
// atomic addition of int64 values Rocksdb already has a UInt64AddOperator, but
// it looks like we do host order versus little endianness, or atleast we use
// inttoBinaryString and vice-versa which may not be same as rocksdb's put
class Int64AddOperator: public rocksdb::AssociativeMergeOperator {
public:
  virtual bool Merge(const rocksdb::Slice& key,
        const rocksdb::Slice* existing_value,
        const rocksdb::Slice& value,
        std::string* new_value,
        rocksdb::Logger* logger) const override {
    int64_t existing = 0;
    if(existing_value != nullptr) {
      existing = binaryStringToInt(existing_value->data());
    }

    int64_t toAdd = binaryStringToInt(value.data());
    int64_t result = existing + toAdd;

    *new_value = intToBinaryString(result);
    return true;
  }

  virtual const char* Name() const override {
    return "Int64AddOperator";
  }
};

class ParallelRocksDBPersistency : public RocksDBPersistency {
public:
  ParallelRocksDBPersistency(const std::string& path,
                             const std::string& options,
                             std::unique_ptr<AckTracker> ackTracker) :
      options_str(options), ackTracker(std::move(ackTracker)),
      RocksDBPersistency() {
    dbpath = path;
    InitializeDB();
  }

  ParallelRocksDBPersistency(const std::string& path,
                             std::string options="") :
      ParallelRocksDBPersistency(path, options,
                                 std::make_unique<HighestAckTracker>()) {}

  ~ParallelRocksDBPersistency() override {
    // Commit the current start & end indices to the databases
    // This is explicitly done to ensure that the serial version of the queue
    // can still read the contents allowing for changes, otherwise
    // MergeOperator changes in journal may not be parsed correctly by the
    // classic serial version of the Queue without explicitly initializing the
    // MergeOperator

    std::cerr << "Destroying ParallelRocksDBPersistency: setting indices: ";
    auto end_index = retrieveCounter("END-INDEX");
    std::cerr << "START-INDEX=" << retrieveCounter("START-INDEX") <<
        " END-INDEX=" << end_index << std::endl;
    rocksdb::WriteBatch batch;
    batch.Put("START-INDEX", intToBinaryString(ackTracker->getStartingIndex()));
    batch.Put("END-INDEX", intToBinaryString(end_index));
    commitBatch(batch);
    auto status  = db->Flush(rocksdb::FlushOptions());
    std::cerr << "Flush Status: " << status.ToString() << std::endl;
  }

  ItemIndex record(const std::vector<std::string> &cmd) override {
    std::string serialization = serializeVector(cmd);
    ItemIndex index = endIndex++;
    std::string key = getKey(index);

    rocksdb::WriteBatch batch;
    batch.Put(key, serialization);
    batch.Merge("END-INDEX", intToBinaryString(1));
    commitBatch(batch);
    return index;
  }

  void popIndex(ItemIndex index) override {
    ackTracker->ackIndex(index);
    rocksdb::WriteBatch batch;
    batch.SingleDelete(getKey(index));
    batch.Put("START-INDEX", intToBinaryString(ackTracker->getStartingIndex()));
    commitBatch(batch);
  }

  void pop() override {
    RocksDBPersistency::pop();
    ackTracker->ackIndex(startIndex);
  }

  ItemIndex getStartingIndex() override {
    return ackTracker->getStartingIndex();
  }

  std::string getType(std::string_view qtype) const override {
    return "ROCKSDB_" + std::string(qtype) + ":" + ackTracker->getType();
  }

private:
  void InitializeDB()
  {
    rocksdb::Options opts = setupOptions();
    rocksdb::DB *ptr = nullptr;
    rocksdb::Status status = rocksdb::DB::Open(opts, dbpath, &ptr);
    db.reset(ptr);

    if(!status.ok()) {
      std::cerr << "Unable to open rocksdb persistent queue: " << status.ToString() << std::endl;
      exit(EXIT_FAILURE);
    }

    startIndex = retrieveCounter("START-INDEX");
    endIndex = retrieveCounter("END-INDEX");
    ackTracker->setStartingIndex(startIndex.load());
  }

  rocksdb::Options makeBaseOptions() {
    rocksdb::Options options;
    rocksdb::BlockBasedTableOptions table_options;
    table_options.filter_policy.reset(rocksdb::NewBloomFilterPolicy(10, false));
    table_options.block_size = 16 * 1024;

    options.table_factory.reset(rocksdb::NewBlockBasedTableFactory(table_options));
    options.create_if_missing = true;
    options.merge_operator.reset(new Int64AddOperator());
    return options;
  }

  rocksdb::Options setupOptions() {
    rocksdb::Options options = makeBaseOptions();
    if (!options_str.empty()) {
      rocksdb::Options new_options; // If the string is invalid this object is invalid...
      rocksdb::ConfigOptions cfg_options;
      cfg_options.ignore_unknown_options = true;
      auto status = rocksdb::GetOptionsFromString(cfg_options, options, options_str, &new_options);
      if (status.ok()) {
        options = std::move(new_options);
        std::string final_db_options_str, final_cf_options_str, final_cf_options_str2;
        auto st = rocksdb::GetStringFromDBOptions(&final_db_options_str, options);
        st = rocksdb::GetStringFromColumnFamilyOptions(&final_cf_options_str, options);
        std::cerr << "QCLIENT: using DB options set from string: " <<  final_db_options_str << std::endl;
        std::cerr << "QCLIENT: using CF options set from string: " << final_cf_options_str << std::endl;
      } else {
        std::cerr << "QCLIENT: skipping invalid string options" << std::endl;
      }
    }
    return options;
  }

  std::string options_str;
  std::unique_ptr<AckTracker> ackTracker;
};

} // namespace qclient


#endif // QCLIENT_PARALLELROCKSDBPERSISTENCY_HH
