// File: AckTracker.hh
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/


#ifndef QCLIENT_ACKTRACKER_HH
#define QCLIENT_ACKTRACKER_HH

#include <atomic>
#include <set>

namespace qclient {

using ItemIndex = int64_t;

/*
 * AckTracker is an abstract class that is used to track the acknowledgements
 * This is primarily needed to have different strategies for tracking acks
 * by the persistency layer. The serial case ensures the acked index is
 * always contigous, which is not the case for parallel queue handlers.
 * Currently we have a simple HighestAckTracker and LowestAckTracker
 * which track the highest-acked index. The lowest ack tracker makes sure that
 * the startingIndex is always the lowest index that is not acked instead of
 * the fact that a given index may be acked already
 */
struct AckTracker {
  virtual void ackIndex(ItemIndex index) = 0;
  virtual bool isAcked(ItemIndex index) = 0;
  virtual void setStartingIndex(ItemIndex index) = 0;
  virtual ItemIndex getStartingIndex() = 0;
  virtual ItemIndex getHighestAckedIndex() = 0;
  virtual std::string getType() const  = 0;
  virtual ~AckTracker() = default;
};

class HighestAckTracker : public AckTracker {
public:
  HighestAckTracker() = default;
  ~HighestAckTracker() override = default;

  void
  ackIndex(ItemIndex index) override
  {
    ItemIndex curr_high = nextIndex.load(std::memory_order_acquire);
    nextIndex.store(std::max(index + 1, curr_high), std::memory_order_release);
  }

  bool
  isAcked(ItemIndex index) override
  {
    return index < nextIndex.load(std::memory_order_acquire);
  }

  ItemIndex
  getStartingIndex() override
  {
    return nextIndex.load(std::memory_order_acquire);
  }

  ItemIndex
  getHighestAckedIndex() override
  {
    return nextIndex.load(std::memory_order_acquire);
  }

  void setStartingIndex(ItemIndex index) override
  {
    nextIndex.store(index, std::memory_order_release);
  }

  virtual std::string getType() const
  {
    return "HIGH";
  }
private:
  std::atomic<ItemIndex> nextIndex{0};
};


/*
 * LowestAckTracker is a simple tracker that tracks the lowest index
 * that is not acked. It uses a set based approach to track all the
 * acked indices and the startingIndex is the lowest index that is not acked
 * ackIndex also calls updateStartingIndex which makes sure to clean up
 * lower indices that are already acked. While this implementation is simple
 * it is not the most efficient as it requires a set to track the acked indices
 * while these can technically be stored as a simple vector of unsigned integers
 * and use a bitmask strategy. This is left as future exercise to the reader ;)
 */
class LowestAckTracker : public AckTracker {
public:
  LowestAckTracker() = default;
  ~LowestAckTracker() override = default;

  void
  ackIndex(ItemIndex index) override
  {
    std::scoped_lock wr_lock(mtx);
    acked.insert(index);
    updateStartingIndex();
  }

  bool
  isAcked(ItemIndex index) override
  {
    std::scoped_lock rd_lock(mtx);
    if (index < startingIndex) {
      return true;
    }
    return acked.find(index) != acked.end();
  }

  ItemIndex
  getStartingIndex() override
  {
    return startingIndex;
  }

  ItemIndex
  getHighestAckedIndex() override
  {
    std::scoped_lock rd_lock(mtx);
    if (acked.empty()) {
      return 0;
    }
    return *acked.rbegin();
  }

  void setStartingIndex(ItemIndex index) override
  {
    std::scoped_lock wr_lock(mtx);
    startingIndex = index;
  }

  virtual std::string getType() const
  {
    return "LOW";
  }
private:
  void
  updateStartingIndex()
  {
    while (!acked.empty() && *acked.begin() == startingIndex) {
      acked.erase(acked.begin());
      ++startingIndex;
    }
  }
  std::set<ItemIndex> acked;
  std::mutex mtx;
  ItemIndex startingIndex{0};
};

inline std::unique_ptr<AckTracker> makeAckTracker(std::string_view tracker_type) {
  if (tracker_type == "HIGH") {
    return std::make_unique<HighestAckTracker>();
  } else if (tracker_type == "LOW") {
    return std::make_unique<LowestAckTracker>();
  }
  return nullptr;
}

} // namespace qclient

#endif // EOS_ACKTRACKER_HH
