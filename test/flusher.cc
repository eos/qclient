// ----------------------------------------------------------------------
// File: flusher.cc
// Author: Abhishek Lekshmanan - CERN
// ----------------------------------------------------------------------

/************************************************************************
  * qclient - A simple redis C++ client with support for redirects       *
  * Copyright (C) 2024 CERN/Switzerland                                  *
  *                                                                      *
  * This program is free software: you can redistribute it and/or modify *
  * it under the terms of the GNU General Public License as published by *
  * the Free Software Foundation, either version 3 of the License, or    *
  * (at your option) any later version.                                  *
  *                                                                      *
  * This program is distributed in the hope that it will be useful,      *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
  * GNU General Public License for more details.                         *
  *                                                                      *
  * You should have received a copy of the GNU General Public License    *
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
  ************************************************************************/

#include "gtest/gtest.h"
#include "QDBFixture.hh"
#include "qclient/BackgroundFlusher.hh"
#include "qclient/RocksDBPersistency.hh"
#include "qclient/ParallelRocksDBPersistency.hh"
#include "qclient/PersistencyLayerBuilder.hh"

using namespace qclient;

TEST_F(QDBInstance, FlusherConstructMemoryPersistency)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::StubInMemoryPersistency<q_item_t>());
  ASSERT_EQ(0, flusher.getStartingIndex());
  ASSERT_EQ(0, flusher.getEndingIndex());
  ASSERT_EQ("MEMORY_SERIAL:LOW", flusher.getPersistencyType());
}

TEST_F(QDBInstance, FlusherConstructRocksDBPersistency)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::RocksDBPersistency(tmp_dir));
  ASSERT_EQ(0, flusher.getStartingIndex());
  ASSERT_EQ(0, flusher.getEndingIndex());
  ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
}

void testPush(qclient::BackgroundFlusher& flusher,
              std::string tag="rocksdb")
{
  auto start_time = std::chrono::high_resolution_clock::now();
  int max_reqs = 10000;
  std::string hash_str = "dict_" + tag + "_single";
  for (int i = 0; i < max_reqs; i++) {
    std::string key = "key" + std::to_string(i);
    std::string val = "val" + std::to_string(i);
    flusher.pushRequest({"HSET", hash_str, key, val});
  }

  EXPECT_EQ(max_reqs, flusher.getEndingIndex());

  while (!flusher.waitForIndex(max_reqs - 1, std::chrono::milliseconds(10))) {
    std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
              << " acknowledged = " << flusher.getAcknowledgedAndClear() << std::endl;
  }
  auto end_time = std::chrono::high_resolution_clock::now();
  auto ms_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
  EXPECT_EQ(max_reqs, flusher.getStartingIndex());
  std::cerr << "Total test time with " << tag << " persistency for "
            << max_reqs << " serial requests:" << ms_elapsed
            << " ms" << std::endl;
}

void testMultiPush(qclient::BackgroundFlusher& flusher,
                   std::string tag="rocksdb",
                   int default_max_reqs = 20000,
                   int default_max_threads = 16)
{
  auto start_time = std::chrono::high_resolution_clock::now();
  int max_reqs = std::getenv("QCL_MAX_REQS") ? atoi(std::getenv("QCL_MAX_REQS")) : default_max_reqs;
  int max_threads = std::getenv("QCL_THREADS") ? atoi(std::getenv("QCL_THREADS")) : default_max_threads;
  ItemIndex startIndex = flusher.getStartingIndex();
  std::cerr << "Starting index = " << startIndex
            << " endingIndex=" << flusher.getEndingIndex() << std::endl;

  auto push_fn = [&flusher, &tag, max_reqs](int thread_id) {
    std::string hash_str = "dict_" + tag +  std::to_string(thread_id);
    for (int i = 0; i < max_reqs; i++) {
      std::string key = "key" + std::to_string(i);
      std::string val = "val" + std::to_string(i);
      flusher.pushRequest({"HSET", hash_str, key, val});
    }
  };

  std::vector<std::thread> threads;
  for (int i=0; i < max_threads; ++i) {
    threads.push_back(std::thread(push_fn, i));
  }

  for (auto& thread : threads) {
    thread.join();
  }
  auto end_time = std::chrono::high_resolution_clock::now();
  auto ms_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
  EXPECT_EQ(max_reqs * max_threads + startIndex, flusher.getEndingIndex());

  while (!flusher.waitForIndex(flusher.getEndingIndex() - 1, std::chrono::milliseconds(100))) {
    std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
              << " acknowledged = " << flusher.getAcknowledgedAndClear()
              << " starting index =" << flusher.getStartingIndex()
              << " ending index =" << flusher.getEndingIndex() << std::endl;
  }

  auto end_time2 = std::chrono::high_resolution_clock::now();
  auto ms_elapsed2 = std::chrono::duration_cast<std::chrono::milliseconds>(end_time2 - end_time).count();
  EXPECT_EQ(max_reqs * max_threads + startIndex, flusher.getStartingIndex());
  std::cerr << "Total test time with " << tag << " persistency for " <<
             max_reqs << " reqs/thread with " << max_threads
            << " threads " << ms_elapsed << ",  " << ms_elapsed2
            << " ms frequency " << (max_reqs * max_threads)/ms_elapsed << " kHz"
            << std::endl;
}

void testJournalReplay(qclient::BackgroundFlusher& flusher,
                       std::string tag="rocksdb")
{
  int max_reqs = 10000;
  std::string hash_str = "dict_" + tag + "_single";
  ItemIndex startIndex = flusher.getStartingIndex();

  for (int i = 0; i < max_reqs; i++) {
    std::string key = "key" + std::to_string(i);
    std::string val = "val" + std::to_string(i);
    flusher.pushRequest({"HSET", hash_str, key, val});
  }

  EXPECT_EQ(max_reqs + startIndex, flusher.getEndingIndex());
}

TEST_F(QDBFlusherInstance, MemoryPersistencypush)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::StubInMemoryPersistency<q_item_t>());
  ASSERT_EQ("MEMORY_SERIAL:LOW", flusher.getPersistencyType());
  testPush(flusher, "memory");
}

TEST_F(QDBFlusherInstance, MemoryPersistencyMultiPush)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::StubInMemoryPersistency<q_item_t>());
  ASSERT_EQ("MEMORY_SERIAL:LOW", flusher.getPersistencyType());
  testMultiPush(flusher, "memory");
}

TEST_F(QDBFlusherInstance, MemoryPersistencyMultiPushLockfree)
{
  auto opts = getQCOpts();
  opts.backpressureStrategy = BackpressureStrategy::RateLimitPendingRequests(1ULL<<22);
  qclient::BackgroundFlusher flusher(members, std::move(opts),
                                     dummyNotifier,
                                     std::make_unique<qclient::StubInMemoryPersistency<q_item_t, true>>(),
                                     qclient::FlusherQueueHandlerT::LockFree);
  ASSERT_EQ("MEMORY_MULTI:LOW", flusher.getPersistencyType());
  testMultiPush(flusher, "memory_lockfree");
}

TEST_F(QDBFlusherInstance, MemoryPersistencyMultiPushLockfreeHighestAck)
{
  auto opts = getQCOpts();
  opts.backpressureStrategy = BackpressureStrategy::RateLimitPendingRequests(1ULL<<22);
  auto ack_tracker = std::make_unique<qclient::HighestAckTracker>();
  qclient::BackgroundFlusher flusher(members, std::move(opts),
                                     dummyNotifier,
                                     std::make_unique<qclient::StubInMemoryPersistency<q_item_t, true>>(std::move(ack_tracker)),
                                     qclient::FlusherQueueHandlerT::LockFree);
  ASSERT_EQ("MEMORY_MULTI:HIGH", flusher.getPersistencyType());
  testMultiPush(flusher, "memory_lockfree_highack");
}

TEST_F(QDBFlusherInstance, NullPeristency)
{
  auto flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members,
                                                                getQCOpts(),
                                                                dummyNotifier,
                                                                "TESTING_NULL_UNSAFE_IN_PROD", RocksDBConfig(tmp_dir));
  ASSERT_EQ("NULL_PERSISTENCY_MULTI:LOW", flusher.getPersistencyType());
  testMultiPush(flusher, "null");
}

TEST_F(QDBFlusherInstance, RocksDBPeristencypush)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::RocksDBPersistency(tmp_dir));
  testPush(flusher, "rocksdb");
}

TEST_F(QDBFlusherInstance, RocksDBPersistencyMultiPush)
{
  qclient::BackgroundFlusher flusher(members, getQCOpts(),
                                     dummyNotifier,
                                     new qclient::RocksDBPersistency(tmp_dir));
  ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
  testMultiPush(flusher, "rocksdb");
}

TEST_F(QDBFlusherInstance, RocksDBPersistencyMultiPushLockfree)
{
  auto opts = getQCOpts();
  opts.backpressureStrategy = BackpressureStrategy::RateLimitPendingRequests(1ULL<<22);
  std::string rocks_options;
  qclient::BackgroundFlusher flusher(members, std::move(opts),
                                     dummyNotifier,
                                     std::make_unique<qclient::ParallelRocksDBPersistency>(tmp_dir, rocks_options),
                                     qclient::FlusherQueueHandlerT::LockFree);
  ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher.getPersistencyType());
  testMultiPush(flusher, "rocksdb_lockfree");
}

TEST_F(QDBFlusherInstance, RocksDBMultiPushBuilder)
{
  std::string rocksdb_options = "enable_pipeline_write=false";
  auto flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                dummyNotifier, "ROCKSDB_MULTI:LOW",
                                                                RocksDBConfig(tmp_dir, rocksdb_options));
  ASSERT_EQ("ROCKSDB_MULTI:LOW", flusher.getPersistencyType());

  testMultiPush(flusher, "rocksdb_lockfree_builder:low");
}


// start with serial rocksdb -> move to parallel -> move back to serial
TEST_F(QDBFlusherInstance, RocksDBRestartsMulti)
{
  {
    qclient::BackgroundFlusher flusher1(
        members, getQCOpts(), dummyNotifier,
        new qclient::RocksDBPersistency(tmp_dir));
    ASSERT_EQ("ROCKSDB", flusher1.getPersistencyType());

    testMultiPush(flusher1, "rocksdb_restart:serial", 10000, 16);
    std::cerr << "Completed first serial rocksdb test" << std::endl;
  }
  {
    auto flusher2 = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                   dummyNotifier, "ROCKSDB_MULTI", RocksDBConfig(tmp_dir));
    ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher2.getPersistencyType());

    testMultiPush(flusher2, "rocksdb_restart:parallel",10000, 16);
    std::cerr << "Completed second parallel rocksdb test" << std::endl;
  }
  {
    std::cerr << "Starting third serial rocksdb test" << std::endl;
    auto flusher3 = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                   dummyNotifier, "ROCKSDB", RocksDBConfig(tmp_dir));
    ASSERT_EQ("ROCKSDB", flusher3.getPersistencyType());
    testMultiPush(flusher3, "rocksdb_restart:serial2",10000, 16);
  }

}

static void printIndices(qclient::BackgroundFlusher& flusher)
{
  std::cerr << "Starting index = " << flusher.getStartingIndex()
            << " endingIndex=" << flusher.getEndingIndex() << std::endl;
}

TEST_F(QDBFlusherInstance, RocksDBPersistencyJournalReplay)
{
  {
    qclient::BackgroundFlusher flusher (members, getQCOpts(), dummyNotifier,
                                       new qclient::RocksDBPersistency(tmp_dir));
    ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
    testJournalReplay(flusher, "rocksdb");
    printIndices(flusher);
  }

  {
    qclient::BackgroundFlusher flusher (members, getQCOpts(), dummyNotifier,
                                       new qclient::RocksDBPersistency(tmp_dir));
    ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
    printIndices(flusher);
    while (!flusher.waitForIndex(flusher.getEndingIndex() - 1, std::chrono::milliseconds(10))) {
      std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
                << " acknowledged = " << flusher.getAcknowledgedAndClear()
                << " starting index =" << flusher.getStartingIndex()
                << " ending index =" << flusher.getEndingIndex() << std::endl;
    }

    ASSERT_EQ(flusher.getStartingIndex(), flusher.getEndingIndex());
  }

}

TEST_F(QDBFlusherInstance, RocksDBMultiPersistencyJournalReplay)
{
  {
    qclient::BackgroundFlusher flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                                        dummyNotifier, "ROCKSDB_MULTI",
                                                                                        RocksDBConfig(tmp_dir));
    ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher.getPersistencyType());
    testJournalReplay(flusher, "rocksdb");
    printIndices(flusher);
  }

  {
    qclient::BackgroundFlusher flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                                        dummyNotifier, "ROCKSDB_MULTI",
                                                                                        RocksDBConfig(tmp_dir));
    printIndices(flusher);
    ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher.getPersistencyType());

    while (!flusher.waitForIndex(flusher.getEndingIndex() - 1, std::chrono::milliseconds(10))) {
      std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
                << " acknowledged = " << flusher.getAcknowledgedAndClear()
                << " starting index =" << flusher.getStartingIndex()
                << " ending index =" << flusher.getEndingIndex() << std::endl;
    }

    ASSERT_EQ(flusher.getStartingIndex(), flusher.getEndingIndex());
  }

}

TEST_F(QDBFlusherInstance, RocksDBMixedJournalReplay)
{
  {
    qclient::BackgroundFlusher flusher (members, getQCOpts(), dummyNotifier,
                                       new qclient::RocksDBPersistency(tmp_dir));
    ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
    testJournalReplay(flusher, "rocksdb");
    printIndices(flusher);
  }

  {
    qclient::BackgroundFlusher flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                                        dummyNotifier, "ROCKSDB_MULTI",
                                                                                        RocksDBConfig(tmp_dir));
    ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher.getPersistencyType());
    printIndices(flusher);
    while (!flusher.waitForIndex(flusher.getEndingIndex() - 1, std::chrono::milliseconds(10))) {
      std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
                << " acknowledged = " << flusher.getAcknowledgedAndClear()
                << " starting index =" << flusher.getStartingIndex()
                << " ending index =" << flusher.getEndingIndex() << std::endl;
    }

    ASSERT_EQ(flusher.getStartingIndex(), flusher.getEndingIndex());
  }
}

TEST_F(QDBFlusherInstance, RocksDBMixedJournalReplay2)
{
  {
    qclient::BackgroundFlusher flusher = qclient::BackgroundFlusherBuilder::makeFlusher(members, getQCOpts(),
                                                                                        dummyNotifier, "ROCKSDB_MULTI",
                                                                                        RocksDBConfig(tmp_dir));
    ASSERT_EQ("ROCKSDB_MULTI:HIGH", flusher.getPersistencyType());
    testJournalReplay(flusher, "rocksdb");
    printIndices(flusher);
  }

  {
    qclient::BackgroundFlusher flusher (members, getQCOpts(), dummyNotifier,
                                       new qclient::RocksDBPersistency(tmp_dir));

    ASSERT_EQ("ROCKSDB", flusher.getPersistencyType());
    printIndices(flusher);
    while (!flusher.waitForIndex(flusher.getEndingIndex() - 1, std::chrono::milliseconds(10))) {
      std::cerr << "total pending=" << flusher.size() << " enqueued = " << flusher.getEnqueuedAndClear()
                << " acknowledged = " << flusher.getAcknowledgedAndClear()
                << " starting index =" << flusher.getStartingIndex()
                << " ending index =" << flusher.getEndingIndex() << std::endl;
    }

    ASSERT_EQ(flusher.getStartingIndex(), flusher.getEndingIndex());
  }
}
